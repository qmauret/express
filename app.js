// DOCS
// https://www.npmjs.com/package/ejs

// GENERAL
const express = require('express');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const app = express();


// VARS
app.locals.title = 'Express TODO APP';
app.locals.url = 'http://localhost:3000';
let todos = [];

// MIDDLEWARES
app.use(bodyParser.json())
.use(bodyParser.urlencoded({ extended: true }))
.use(cookieSession({
    name: 'session',
    keys: ['key1']
}));

// ROUTING
app.use((req, res, next) => {
    if (!req.session.todos) {
        req.session.todos = todos;
    }
    next();
}).get('/', (req, res) => {
    res.redirect('/todos');
})
.get('/todos', (req, res) => {
    if (req.session.todos) todos = req.session.todos;
    res.render('todos.ejs', {title: app.locals.title, siteUrl: app.locals.url, todos: todos});
}).post('/todos/add', (req, res) => {
    if (req.body) {
        if (req.body.todo && req.body.todo !== '') {
            addTodo(req.body.todo);
        }
        req.session.todos = todos;
        res.redirect('/todos');
    } else return res.sendStatus(400);
}).get('/todos/delete/:id', (req, res) => {
    if (req.body) {
        if (req.params.id && todos[req.params.id]) {
            deleteTodo(todos[req.params.id]);
        }
        req.session.todos = todos;
        res.redirect('/todos');
    } else return res.sendStatus(400);
}).use((req, res, next) => {
    res.status(404).send('Page introuvable !');
});

/**
 * AddTodo
 * @param {* string} todo 
 */
var addTodo = (todo) => {
    if (todos.find(todoItem => todoItem === todo) == undefined) {
        todos.push(todo);
    }
};

/**
 * DeleteTodo
 * @param {* string} todo 
 */
var deleteTodo = (todo) => {
    todos = todos.filter(todoItem => todoItem !== todo);
};

/**
 * Start Server
 */
app.listen(3000, () => {
    console.log('express app is listening on port 3000');
});